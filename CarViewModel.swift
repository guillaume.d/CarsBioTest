//
//  CarViewModel.swift
//  CarsBioTest
//
//  Created by Guillaume Dadouche on 03/05/2017.
//  Copyright © 2017 projetDemineur. All rights reserved.
//
import ReactiveCocoa
import ReactiveSwift
import Result
import UIKit
import Starscream
import Foundation

class CarViewModel {
    
    
    // MARK - Properties
    
    var socket: WebSocket?
    var carsList: MutableProperty<[Car]> = MutableProperty([])
    var carNameArray = [String]()
    
    var info: NSData?
    let jsonArray = ["Type" : "infos"]
    var start: NSData?
    
    
    init() {
        
        socket = WebSocket(url: URL(string: "wss://pbbouachour.fr/openSocket")!)
        
        if let socket = self.socket {
            socket.delegate = self
            socket.connect()
        }
        
        info = self.jsonToNSData(json: jsonArray as AnyObject )
    }
    
    deinit {
        print("deinit")
        socket?.disconnect(forceTimeout: 0)
        socket?.delegate = nil
    }
    
    
    func jsonToNSData(json: AnyObject) -> NSData?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
    func startTheRace() {
        print("🏁start the Race .... ")
        
        for name in self.carNameArray {
            print("\(name) -> GO ...")
            
            let startRaceJason = ["Type" : "start", "UserToken" : 42, "Payload":["Name" : name] ] as [String : Any]
            start = self.jsonToNSData(json: startRaceJason as AnyObject)
            
            if let socket = self.socket {
                socket.write(data: (start!) as Data)
            }
        }
    }
    
    func stopTheRace() {
        
        let stopJson =  ["Type" : "stop" , "UserToken" : 42] as [String : Any]
        let stop = self.jsonToNSData(json: stopJson as Any as AnyObject)
        
        for _ in 0..<self.carNameArray.count {
            
            if let socket = self.socket {
                socket.write(data: (stop!) as Data)
            }
        }
    }
    
    func roundDouble(number: Double) -> Double {
        let num = Double(round(number*100)/100)
        return num
    }
}

// Mark - Extension

extension CarViewModel: WebSocketDelegate {
    
    public func websocketDidConnect(socket: Starscream.WebSocket) {
        print("didConnect")
        
        socket.write(data: info! as Data)
    }
    
    public func websocketDidDisconnect(socket: Starscream.WebSocket, error: NSError?) {
        print("didDisconnect")
        
    }
    
    public func websocketDidReceiveMessage(socket: Starscream.WebSocket, text: String) {
        print("receved message ...")
        print(text)
        
        guard let data = text.data(using: .utf16),
            let _ = try? JSONSerialization.jsonObject(with: data) else {return}
        
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let jsonResult = jsonResult as? NSArray {
                print("Array")
                
                for json in jsonResult {
                    
                    let productData = json as! [String:Any]
                    
                    guard let brand = productData["Brand"] as? String else {return}
                    guard let name = productData["Name"] as? String else {return}
                    guard let speedMax = productData["SpeedMax"] else {return}
                    guard let cv = productData["Cv"] else {return}
                    guard let currentSpeed = productData["CurrentSpeed"] as? Double  else {return}
                    
                    let car = Car(brand: brand, name: name, speedMax: speedMax as! Int, cv: cv as! Int, currentSpeed: currentSpeed)
                    
                    self.carNameArray.append(name)
                    self.carsList.value.append(car)
                }
            }
            
            if let dicoResponse = jsonResult as? [String: Any] {
                
                guard let brand = dicoResponse["Brand"] as? String else {return}
                guard let currentSpeed = dicoResponse["CurrentSpeed"] as? Double  else {return}

                self.updateCarSpeed(brand: brand, speed: self.roundDouble(number: currentSpeed))
            }
            
        } catch let error as NSError {
            print("Error in parsing carList.json: \(error.localizedDescription)")
        }
        
    }
    
    public func websocketDidReceiveData(socket: Starscream.WebSocket, data: Data) {
        print("got some data: \(data.count)")
    }
    
    public func updateCarSpeed(brand: String, speed: Double) {
        print("update car")
        
        for (index, value) in self.carsList.value.enumerated() {
            if (value.brand == brand) {
                value.currentSpeed = speed
                let newCar = value
                self.carsList.value[index] = newCar
            }
        }
    }
}







