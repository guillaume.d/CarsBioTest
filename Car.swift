//
//  Car.swift
//  CarsBioTest
//
//  Created by Guillaume Dadouche on 03/05/2017.
//  Copyright © 2017 projetDemineur. All rights reserved.
//
import ReactiveCocoa
import ReactiveSwift
import Foundation

class Car {
    
    // MARK - Properties
    var brand: String
    var name: String
    var speedMax: Int
    var cv: Int
    var currentSpeed: Double
    
    init(brand: String, name: String, speedMax: Int, cv: Int, currentSpeed: Double) {
        
        self.brand = brand
        self.name = name
        self.speedMax = speedMax
        self.cv = cv
        self.currentSpeed = currentSpeed
    }
}
