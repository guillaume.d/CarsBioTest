//
//  CarCell.swift
//  CarsBioTest
//
//  Created by Guillaume Dadouche on 08/05/2017.
//  Copyright © 2017 projetDemineur. All rights reserved.
//

import UIKit

class CarCell: UITableViewCell {
    
    // MARK - IBOutlets
    
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var cvLabel: UILabel!
    @IBOutlet weak var currentSpeedLabel: UILabel!
    @IBOutlet weak var currentSpeedTitleLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        
        brandLabel.layer.cornerRadius = 8.0
        brandLabel.layer.borderWidth = 1.0
        brandLabel.layer.borderColor = UIColor.white.cgColor
        
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.white.cgColor
        self.backgroundColor = UIColor.init(red: 70, green: 86, blue: 116)
        
        self.brandLabel.backgroundColor = UIColor.init(red: 164, green: 142, blue: 133)
        
        self.currentSpeedTitleLabel.font =
            UIFont(name: "CloseRace", size: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
