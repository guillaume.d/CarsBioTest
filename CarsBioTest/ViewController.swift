//
//  ViewController.swift
//  CarsBioTest
//
//  Created by Guillaume Dadouche on 03/05/2017.
//  Copyright © 2017 projetDemineur. All rights reserved.
//
import ReactiveCocoa
import ReactiveSwift
import Result
import UIKit

enum logo: String {
    case aston = "logoAston"
    case audi = "logoAudi"
    case bmw = "logoBMW"
    case ferrari = "logoFerrari"
    case jag = "logoJaguard"
    case peugeot = "logoPeugeot"
    case porsche = "logoPorsche"
    case renault = "logoRenault"
    case def = "defaultCarImage"
}

class ViewController: UIViewController {
    
    // MARK - Properties
    var carVM: CarViewModel?
    var carsArray: MutableProperty<[Car]> = MutableProperty([])
    
    // MARK - IBOutlets
    @IBOutlet weak var carsTableView: UITableView!

    @IBOutlet weak var stopButton: MyButton!
    @IBOutlet weak var startButton: MyButton!
    
    // MARK - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "THE RACE 🏆"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(red: 219, green: 219, blue: 219)
        
        startButton.titleLabel?.font =
            UIFont(name: "CloseRace", size: 20)
        
        stopButton.titleLabel?.font =
            UIFont(name: "CloseRace", size: 20)
        

        self.carsTableView.dataSource = self
        
        carVM = CarViewModel()
        
        self.carsArray <~ carVM!.carsList
        
        carsArray.signal.observeValues {
            array in
            print("🚩Array did change")
            
            self.carsTableView.reloadData()
        }
        
    }
    
    // MARK - Action
    
    @IBAction func didTapStartRace(_ sender: UIButton) {
        carVM?.startTheRace()
    }
    
    @IBAction func didTapStopRace(_ sender: UIButton) {
        carVM?.stopTheRace()
    }
    
    func carlogo(name: String) -> String {
        
        var logoName: String?
        
        switch name {
        case "Aston Martin":
            logoName = logo.aston.rawValue
        case "Peugeot":
            logoName = logo.peugeot.rawValue
        case "Ferrari":
            logoName = logo.ferrari.rawValue
        case "Porsche":
            logoName = logo.porsche.rawValue
        case "BMW":
            logoName = logo.bmw.rawValue
        case "Audi":
            logoName = logo.audi.rawValue
        case "Jaguar":
            logoName = logo.jag.rawValue
        default:
            logoName = logo.def.rawValue
        }
        
        return logoName!
    }
    
}

// MARK - Extensions

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.carsArray.value.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.carsArray.value.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = carsTableView.dequeueReusableCell(withIdentifier: "CarCell") as! CarCell
        let car = self.carsArray.value[indexPath.row]
        
        cell.brandLabel.text = car.brand
        cell.carModelLabel.text = car.name
        cell.maxSpeedLabel.text = "\(car.speedMax)"
        cell.cvLabel.text = "\(car.cv)"
        cell.currentSpeedLabel.text =  "\(car.currentSpeed)"
        
        
        let logoName = self.carlogo(name: car.brand)
        cell.logo.image = UIImage(named: logoName)
        
        return cell
    }
}

//extension ViewController: UITableViewDelegate {
//    
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        print("height")
//        return 10
//    }
//}
